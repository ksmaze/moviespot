from bs4 import BeautifulSoup
import lxml.html
import urllib2
import os
import codecs
import sqlite3

conn = sqlite3.connect("movie100k.db")
c = conn.cursor()
c.execute("SELECT id,imdb_url FROM url_table WHERE id>1500 AND id<1701")
movie_url_list = c.fetchall()

movie_director = []
keyword_file = codecs.open("keyword6", "w+", "utf-8")

for movie in movie_url_list:
    req = urllib2.Request(movie[1], headers={'User-Agent':'Magic Browser'})
    html_doc = urllib2.urlopen(req)
    soup = BeautifulSoup(html_doc, 'lxml')

    overview_div = soup.find("div", id="titleStoryLine")
    if overview_div != None:
        keyword_tag = overview_div.find("div", itemprop="keywords")
        if keyword_tag != None:
            keywords = ""
            keyword_links = keyword_tag.find_all("a")
            for link in keyword_links:
                keyword = link.contents[0][1:]
                if keyword != "ee more":
                    keywords = keywords + keyword + ","
            keyword_file.write('|'.join([str(movie[0]), keywords[0:-1]])+os.linesep)
            print str(movie[0]) + " " + keywords[0:-1] + " pass."
        else:
            print "no keyword" + str(movie)
    else:
        print "no story line" + str(movie)

keyword_file.close()

conn.close()
