import urllib, urllib2
from bs4 import BeautifulSoup
import lxml.html
import os
import codecs

#get list of movies' URL
srcfileName = '../data/100k-sep/100k-moive-4'
outputFile = codecs.open('100K-movie-url-4', 'w+', 'utf-8')
srcfile = open(srcfileName)
movieList = srcfile.readlines()
srcfile.close()
urlPrefix = 'http://www.imdb.com'
for item in movieList:
	splitItem = item.split("|")

	#make search url
	searchUrlPrefix = 'http://www.imdb.com/find?q='
	searchUrlSuffix = '&s=tt'
	movieKeyword = urllib.quote(splitItem[1][0:-7])
	releaseTime = splitItem[1][-6:-1] +')'
	searchUrl = searchUrlPrefix + movieKeyword + searchUrlSuffix
	
	#open search page
	req = urllib2.Request(searchUrl, headers={'User-Agent':'Magic Browser'})
	html_doc = urllib2.urlopen(req)
	soup = BeautifulSoup(html_doc,'lxml')
	
	#get and format research result
	searchResult = soup.select('td[class="result_text"]')
	url = u''
	title = u''
	
	#no searching result
	if len(searchResult) == 0:
		title = splitItem[1][0:-7]
		print 'no result: ' + splitItem[0]
	else:
		for result in searchResult:
			text = result.contents[2]
			if cmp(text[2:6], releaseTime[1:5]) == 0:
				url = urlPrefix + result.contents[1]['href']
				title = result.contents[1].contents[0]
				break
		if url == '':
			url = urlPrefix + searchResult[0].contents[1]['href']
			title = searchResult[0].contents[1].contents[0]
			text = searchResult[0].contents[2]
			releaseTime = text[1:7]
			print  splitItem[0] + ' not found set as ' + title + ' ' + releaseTime
	
	line = '|'.join([splitItem[0], title, releaseTime[1:5], url]) + os.linesep
	print splitItem[0] + ' ' + title + ' done.'
	outputFile.write(line)
	
	
outputFile.close()
	
	
