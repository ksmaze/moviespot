from bs4 import BeautifulSoup
import lxml.html
import urllib2
import os
import codecs
import sqlite3

conn = sqlite3.connect("movie100k.db")
c = conn.cursor()
c.execute("SELECT id,imdb_url FROM url_table WHERE id>1533 AND id<1700")
movie_url_list = c.fetchall()

rating_id = 0
movie_rating = []
rating_file = codecs.open("rating3", "w+", "utf-8")

for movie in movie_url_list:
    req = urllib2.Request(movie[1], headers={'User-Agent':'Magic Browser'})
    html_doc = urllib2.urlopen(req)
    soup = BeautifulSoup(html_doc, 'lxml')

    overview_table = soup.find("td", id="overview-top")
    rating_tag = overview_table.find("div", "titlePageSprite")
    if rating_tag != None:
        rating = rating_tag.contents[0][1:-1]
        rating_file.write('|'.join([str(movie[0]), rating])+os.linesep)
        print str(movie[0]) + " " + rating + " pass."
    else:
        rating_file.write('|'.join([str(movie[0]), ""])+os.linesep)
        print "no director" + str(movie)

rating_file.close()

conn.close()
