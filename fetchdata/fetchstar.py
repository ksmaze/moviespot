from bs4 import BeautifulSoup
import lxml.html
import urllib2
import os
import codecs
import sqlite3

conn = sqlite3.connect("movie100k.db")
c = conn.cursor()
c.execute("SELECT id,imdb_url FROM url_table WHERE id>1500 AND id<1701")
movie_url_list = c.fetchall()

movie_actors = []
actors_file = codecs.open("actors6", "w+", "utf-8")

for movie in movie_url_list:
    req = urllib2.Request(movie[1], headers={'User-Agent':'Magic Browser'})
    html_doc = urllib2.urlopen(req)
    soup = BeautifulSoup(html_doc, 'lxml')

    overview_table = soup.find("table", id="title-overview-widget-layout")
    actors_tag = overview_table.find_all("span", itemprop="actors")
    if actors_tag != None:
        actor_name = []
        for actor in actors_tag:
            actor_link = actor.find("a", itemprop="url")
            actor_name.append(actor_link.contents[0])
        actors_file.write('|'.join([str(movie[0]), '|'.join(actor_name)])+os.linesep)
        print str(movie[0]) + " " + str(actor_name) + " pass."
    else:
        actors_file.write('|'.join([str(movie[0]), "NULL"])+os.linesep)
        print "no actors" + str(movie)

actors_file.close()

conn.close()
