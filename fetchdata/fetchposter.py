import urllib
import urllib2
import lxml.html
import threading
import Queue
from bs4 import BeautifulSoup
import os

class AppUrlopener(urllib.FancyURLopener):
	version = "Mozilla/5.0"
	
exitFlag = 0
	
class DownloadingThread (threading.Thread):
	def __init__(self, q):
		self.q = q
		threading.Thread.__init__(self)
	def run(self):
		download(self.q)
		
def download(q):
	while not exitFlag:
		queueLock.acquire()
		if not workQueue.empty():
			downloadInfo = q.get()
			queueLock.release()
			# download from the url
			urllib.urlretrieve(imgUrl, imgName)
		else:
			queueLock.release()
		
	
def getImageFormat( url ):
	splitedUrl = url.split('.')
	return splitedUrl[-1]

#get list of movies' URL
srcfileName = '100K-movie-url-4'
srcfile = open(srcfileName)
movieList = srcfile.readlines()
movieUrlList = []
for item in movieList:
	splitItem = item.split("|")
	movieUrlList.append([splitItem[0], splitItem[3]])

srcfile.close()

#create thread and locks
queueLock = threading.Lock()
workQueue = Queue.Queue(len(movieUrlList))
thread = DownloadingThread(workQueue)
thread.start()

#fetching items from pages and save to database
urllib._urlopener = AppUrlopener()
for movieUrl in movieUrlList:
	if cmp(movieUrl[1], 'na'+os.linesep) == 0:
		print 'no result: ' + movieUrl[0]
		continue
	req = urllib2.Request(movieUrl[1], headers={'User-Agent':'Magic Browser'})
	html_doc = urllib2.urlopen(req)
	soup = BeautifulSoup(html_doc,'lxml')

	#download img
	imgTag = soup.select('img[title$="Poster"]')
	if len(imgTag) == 1:
		imgUrl = imgTag[0]['src']
		
		#make the filename of img
		imgFormat = getImageFormat(imgUrl)
		imgName = 'posters/' + movieUrl[0] + '.' + imgFormat
		
		#put filename and url into work queue
		queueLock.acquire()
		workQueue.put([imgName, imgUrl])
		queueLock.release()
	else:
		print 'failed to fetch ' + movieUrl[0]
		
while not workQueue.empty():
	pass
	
exitFlag = 1

thread.join()
		
		
		
		
		
		
		
		
		
		
		
		
