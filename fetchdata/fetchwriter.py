from bs4 import BeautifulSoup
import lxml.html
import urllib2
import os
import codecs
import sqlite3

conn = sqlite3.connect("movie100k.db")
c = conn.cursor()
c.execute("SELECT id,imdb_url FROM url_table WHERE id>1500 AND id<1701")
movie_url_list = c.fetchall()

movie_writer = []
writer_file = codecs.open("writer6", "w+", "utf-8")

for movie in movie_url_list:
    req = urllib2.Request(movie[1], headers={'User-Agent':'Magic Browser'})
    html_doc = urllib2.urlopen(req)
    soup = BeautifulSoup(html_doc, 'lxml')

    overview_table = soup.find("table", id="title-overview-widget-layout")
    writer_tag = overview_table.find("span", itemprop="writer")
    if writer_tag != None:
        writer_link = writer_tag.find("a", itemprop="url")
        writer_name = writer_link.contents[0]
        writer_file.write('|'.join([str(movie[0]), writer_name])+os.linesep)
        print str(movie[0]) + " " + writer_name + " pass."
    else:
        writer_file.write('|'.join([str(movie[0]), "NULL"])+os.linesep)
        print "no writer" + str(movie)

writer_file.close()

conn.close()
