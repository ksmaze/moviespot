from bs4 import BeautifulSoup
import lxml.html
import urllib2
import os
import codecs
import sqlite3

conn = sqlite3.connect("movie100k.db")
c = conn.cursor()
c.execute("SELECT id,imdb_url FROM url_table WHERE id>1500 AND id<1701")
movie_url_list = c.fetchall()

rating_file = codecs.open("rating6", "w+", "utf-8")

for movie in movie_url_list:
    req = urllib2.Request(movie[1], headers={'User-Agent':'Magic Browser'})
    html_doc = urllib2.urlopen(req)
    soup = BeautifulSoup(html_doc, 'lxml')

    overview_td = soup.find("td", id="overview-top")
    rating_tag = overview_td.find("span", itemprop="contentRating")
    if rating_tag != None:
        try:
            rating = rating_tag['class'][0]
        except KeyError:
            rating = rating_tag.contents[0]
        except:
            rating = ''
            print 'no content rating' + str(movie)
        rating_file.write('|'.join([str(movie[0]), rating])+os.linesep)
        print str(movie[0]) + " " + rating + " pass."
    else:
        rating_file.write('|'.join([str(movie[0]), "NULL"])+os.linesep)
        print "no content rating" + str(movie)

rating_file.close()

conn.close()
