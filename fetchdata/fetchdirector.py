from bs4 import BeautifulSoup
import lxml.html
import urllib2
import os
import codecs
import sqlite3

conn = sqlite3.connect("movie100k.db")
c = conn.cursor()
c.execute("SELECT id,imdb_url FROM url_table WHERE id>1500 AND id<1701")
movie_url_list = c.fetchall()

director_id = 0
movie_director = []
director_file = codecs.open("director6", "w+", "utf-8")

for movie in movie_url_list:
    req = urllib2.Request(movie[1], headers={'User-Agent':'Magic Browser'})
    html_doc = urllib2.urlopen(req)
    soup = BeautifulSoup(html_doc, 'lxml')

    overview_table = soup.find("table", id="title-overview-widget-layout")
    director_tag = overview_table.find("span", itemprop="director")
    if director_tag != None:
        director_link = director_tag.find("a", itemprop="url")
        director_name = director_link.contents[0]
        director_file.write('|'.join([str(movie[0]), director_name])+os.linesep)
        print str(movie[0]) + " " + director_name + " pass."
    else:
        director_file.write('|'.join([str(movie[0]), "NULL"])+os.linesep)
        print "no director" + str(movie)

director_file.close()

conn.close()
