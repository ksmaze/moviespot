from bs4 import BeautifulSoup
import lxml.html
import urllib2
import os
import codecs
import sqlite3

conn = sqlite3.connect("movie100k.db")
c = conn.cursor()
c.execute("SELECT id,imdb_url FROM url_table WHERE id>1500 AND id<1701")
movie_url_list = c.fetchall()

movie_genres = []
genres_file = codecs.open("genre6", "w+", "utf-8")

for movie in movie_url_list:
    req = urllib2.Request(movie[1], headers={'User-Agent':'Magic Browser'})
    html_doc = urllib2.urlopen(req)
    soup = BeautifulSoup(html_doc, 'lxml')

    storyline_div = soup.find("div", id="titleStoryLine")
    genre_tag = storyline_div.find("div", itemprop="genre")
    if genre_tag != None:
        genre_list = []
        genre_links = genre_tag.find_all("a")
        for genre_link in genre_links:
            genre_list.append(genre_link.contents[0][1:])
        genres_file.write('|'.join([str(movie[0]), '|'.join(genre_list)])+os.linesep)
        print str(movie[0]) + " " + str(genre_list) + " pass."
    else:
        genres_file.write('|'.join([str(movie[0]), ""])+os.linesep)
        print "no genres" + str(movie)

genres_file.close()

conn.close()
