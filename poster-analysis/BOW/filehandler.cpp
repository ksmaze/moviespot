#include "filehandler.h"


string getFileNameWithoutExtension(string filename)
{
	size_t found;
	string file(filename);
	found = filename.rfind(".");
	if (found != string::npos)	{
		return file.substr(0, found);
	}
}

string setExtension(string filename, string extension)
{
	string newfile(filename);
	newfile = getFileNameWithoutExtension(newfile);
	return newfile.append(".").append(extension);
}