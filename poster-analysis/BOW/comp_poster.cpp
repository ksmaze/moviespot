#include "comp_poster.h"
#include "calc_poster.h"

void drawhisto_hsv(int argc, char** argv)
{
	Mat src_base;
	src_base = imread(argv[1]);

	/// Draw for each channel
	CHistLib Histogram;
	Mat HistImageBGR;
	MatND HistB;
	MatND HistG;
	MatND HistR;
	Mat src_temp;
	cvtColor(src_base, src_temp, CV_BGR2HSV);
	//Histogram.SetBinCount(20);
	//Histogram.SetDrawSpreadOut(true);
	//Histogram.SetHistImageHeight(400);
	Histogram.ComputeHistogramBGR(src_temp, HistB, HistG, HistR);
	Histogram.DrawHistogramBGR(HistB, HistG, HistR, HistImageBGR);


	imshow("1", HistImageBGR);
	imwrite("1.hsv.jpg", HistImageBGR);
	waitKey();

	printf( "\nDone \n" );
}

void drawhisto_bgr(int argc, char** argv)
{
	Mat src_base;
	src_base = imread(argv[1]);

	/// Draw for each channel
	CHistLib Histogram;
	Mat HistImageBGR;
	MatND HistB;
	MatND HistG;
	MatND HistR;

	//Histogram.SetBinCount(20);
	//Histogram.SetDrawSpreadOut(true);
	//Histogram.SetHistImageHeight(400);
	Histogram.ComputeHistogramBGR(src_base, HistB, HistG, HistR);
	Histogram.DrawHistogramBGR(HistB, HistG, HistR, HistImageBGR);


	imshow("1", HistImageBGR);
	imwrite("1.bgr.jpg", HistImageBGR);
	waitKey();

	printf( "\nDone \n" );
}

void print_histo(int argc, char** argv)
{
	int i, j;
	Mat src_base;
	src_base = imread(argv[1]);
	int h_size = 5, w_size = 5;
	int b_bins = 20, g_bins = 20, r_bins = 20;
	int histSize[] = {b_bins, g_bins, r_bins};

	float b_ranges[] = {0, 256}, g_ranges[] = {0, 256}, r_ranges[] = {0, 256};
	const float* ranges[] = {b_ranges, g_ranges, r_ranges};

	int channels[] = { 0, 1, 2 };

	/// Histograms
	MatND hist_base;
	MatND hist_save;
	double base_base[4] = {0};
	int h_range = src_base.rows/h_size;
	int w_range = src_base.cols/w_size;
	for (i = 0; i < h_size; i++){
		for (j = 0; j < w_size; j++){
			int h_offset = (i+1)*h_range<src_base.rows?h_range:src_base.rows-i*h_range;
			int w_offset = (j+1)*w_range<src_base.cols?w_range:src_base.cols-j*w_range;
			calcHist(&(src_base(Rect(j*w_range, i*h_range, w_offset, h_offset))), 1, channels, Mat(), hist_base, 3, histSize, ranges, true, true);
		}
	}
	save_to_file(hist_base, "1.hist.txt");
	load_from_file(hist_save, "1.hist.txt");
}




void save_to_file(MatND & hist, char* file)
{
	//FILE *writer = fopen(file, "w");
	//fprintf(writer, "{%d,%d,%d,%d,%d,%d,%d,%d}", hist.flags, hist.dataend-hist.datastart, hist.size.p[0],hist.size.p[1],hist.size.p[2],hist.step.p[0],hist.step.p[1],hist.step.p[2]);
	//for (uchar *start = hist.datastart; start!=hist.dataend; start++)
	//{
	//	fputc(*start, writer);
	//}
	//fclose(writer);
	FileStorage fs(file, FileStorage::WRITE);
	fs <<"histogram" <<hist;
	fs.release();
}
void load_from_file(MatND & hist, char* file)
{
	//FILE *reader = fopen(file, "r");
	//int size;
	//hist.size.p = (int*) malloc(4*sizeof(int));
	//hist.step.p = (size_t*) malloc(4*sizeof(int));
	//hist.refcount = (int *) malloc(sizeof(int));
	//hist.dims = -1;
	//hist.cols = -1;
	//*(hist.refcount) = 1;
	//fscanf(reader, "{%d,%d,%d,%d,%d,%d,%d,%d}", &hist.flags, &size, hist.size.p ,hist.size.p+1,hist.size.p+2,hist.step.p,hist.step.p+1,hist.step.p+2);
	//hist.data = (uchar*) malloc(size);
	//hist.datastart = hist.data;
	//hist.dataend = hist.datastart+size;
	//hist.datalimit = hist.dataend;
	//for (uchar*ch = hist.datastart; ch != hist.dataend; ch++)
	//{
	//	ch = (uchar*)fgetc(reader);
	//}
	FileStorage fs(file, FileStorage::READ);
	fs["histogram"] >> hist;
	fs.release();
}

void save_bgr_bow(char * filename, int h_size, int w_size)
{
	int i, j,k;
	Mat src_base;
	vector<Mat> bgr_planes;
	if (strstr(filename, "txt") != NULL)
		return;
	src_base = imread(filename);
	string output(filename);
	output = setExtension(output, "txt");
	ofstream outputfile;
	outputfile.open(output);

	split( src_base, bgr_planes );
	//int h_size = 16, w_size = 12;
	int present[3000] = {0};
	int histSize[] = {5};


	float range[] = {0, 256};
	const float* ranges[] = {range};

	int channels[] = { 0};

	/// Histograms
	vector<int> b_hist(histSize[0]), g_hist(histSize[0]), r_hist(histSize[0]);
	double base_base[4] = {0};
	int h_range = src_base.rows/h_size;
	int w_range = src_base.cols/w_size;
	for (i = 0; i < h_size; i++){
		for (j = 0; j < w_size; j++){
			int h_offset = (i+1)*h_range<src_base.rows?h_range:src_base.rows-i*h_range;
			int w_offset = (j+1)*w_range<src_base.cols?w_range:src_base.cols-j*w_range;
			calc_hist(&((bgr_planes[0])(Rect(j*w_range, i*h_range, w_offset, h_offset))), b_hist, histSize[0], range);
			calc_hist(&((bgr_planes[1])(Rect(j*w_range, i*h_range, w_offset, h_offset))), g_hist, histSize[0], range);
			calc_hist(&((bgr_planes[2])(Rect(j*w_range, i*h_range, w_offset, h_offset))), r_hist, histSize[0], range);
			//calcHist( &((bgr_planes[0])(Rect(j*w_range, i*h_range, w_offset, h_offset))), 1, 0, Mat(), b_hist, 1, histSize, ranges, true, false );
			//calcHist( &((bgr_planes[1])(Rect(j*w_range, i*h_range, w_offset, h_offset))), 1, 0, Mat(), g_hist, 1, histSize, ranges, true, false );
			//calcHist( &((bgr_planes[2])(Rect(j*w_range, i*h_range, w_offset, h_offset))), 1, 0, Mat(), r_hist, 1, histSize, ranges, true, false );
			//imshow("1", (bgr_planes[0])(Rect(j*w_range, i*h_range, w_offset, h_offset)));
			//waitKey(0);
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*histSize[0]*3+k] = b_hist[k];
			}
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*histSize[0]*3+histSize[0]+k] = g_hist[k];
			}
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*histSize[0]*3+histSize[0]*2+k] = r_hist[k];
			}
		}
	}

	for( i = 0; i < h_size*w_size*3*histSize[0]; i++ )
	{
		outputfile <<present[i] <<",";
	}
	outputfile.close();
}

void calc_edge_hist( Mat &src_gray, vector<int> e_hist, int histSize, float range ) 
{
	throw std::exception("The method or operation is not implemented.");
}

void save_feature(char * filename, int h_size, int w_size)
{
	int i, j,k;
	Mat src_base, src_gray, dst;
	vector<Mat> bgr_planes;
	if (strstr(filename, "txt") != NULL)
		return;
	src_base = imread(filename);
	string output(filename);
	output = setExtension(output, "txt");
	ofstream outputfile;
	outputfile.open(output);

	cvtColor(src_base, src_gray, CV_BGR2GRAY);
	
	split( src_base, bgr_planes );
	//int h_size = 16, w_size = 12;
	
	int histSize[] = {5};
	int *present = new int[h_size*w_size*(histSize[0]*3+5)+1];

	float range[] = {0, 256};
	const float* ranges[] = {range};
	float thresholds[] = {100,100,100,100,100};

	int channels[] = { 0};

	/// Histograms
	vector<int> b_hist(histSize[0]), g_hist(histSize[0]), r_hist(histSize[0]), e_hist(5);
	double base_base[4] = {0};
	int h_range = src_base.rows/h_size;
	int w_range = src_base.cols/w_size;
	for (i = 0; i < h_size; i++){
		for (j = 0; j < w_size; j++){
			int h_offset = (i+1)*h_range<src_base.rows?h_range:src_base.rows-i*h_range;
			int w_offset = (j+1)*w_range<src_base.cols?w_range:src_base.cols-j*w_range;
			calc_hist(&((bgr_planes[0])(Rect(j*w_range, i*h_range, w_offset, h_offset))), b_hist, histSize[0], range);
			calc_hist(&((bgr_planes[1])(Rect(j*w_range, i*h_range, w_offset, h_offset))), g_hist, histSize[0], range);
			calc_hist(&((bgr_planes[2])(Rect(j*w_range, i*h_range, w_offset, h_offset))), r_hist, histSize[0], range);
			calc_edge_hist(src_gray(Rect(j*w_range, i*h_range, w_offset, h_offset)), e_hist, thresholds);
			//imshow("1", (bgr_planes[0])(Rect(j*w_range, i*h_range, w_offset, h_offset)));
			//waitKey(0);
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*(histSize[0]*3+5)+k] = b_hist[k];
			}
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*(histSize[0]*3+5)+histSize[0]+k] = g_hist[k];
			}
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*(histSize[0]*3+5)+histSize[0]*2+k] = r_hist[k];
			}
			for (k = 0; k < 5; k++)
			{
				present[(i*w_size+j)*(histSize[0]*3+5)+histSize[0]*3+k] = e_hist[k];
			}
		}
	}

	for( i = 0; i < h_size*w_size*(3*histSize[0]+5); i++ )
	{
		outputfile <<present[i] <<",";
	}
	outputfile.close();

}