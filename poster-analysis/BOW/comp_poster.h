#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <fstream>
#include <iostream>
#include "histLib.h"
#include "filehandler.h"

using namespace std;
using namespace cv;

void drawhisto_hsv(int argc, char** argv);
void drawhisto_bgr(int argc, char** argv);
void print_histo(int argc, char** argv);

//////////////////////////////////////////////////////////////////////////
void save_to_file(MatND &, char* );
void load_from_file(MatND &, char* );
void save_bgr_bow(char * filename, int h_size, int w_size);
void save_feature(char * filename, int h_size, int w_size);