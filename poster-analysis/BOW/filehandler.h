#ifndef FILEHANDLER
#include <iostream>
#include <string>
#include <cstringt.h>
#include <stdlib.h>
#include <stdio.h>


using namespace std;

//give file path, return filetype

//give file path, return filename

//give file path, return directory

//give file path, return with prefix

//give file path, return filename without extension
string getFileNameWithoutExtension(string filename);

//give file path, return filename with new extension
string setExtension(string filename, string extension);

//give file path, return with suffix



#endif // FILEHANDLER
