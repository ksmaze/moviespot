#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <hash_map>

using namespace std;
using namespace cv;


void calc_hist(const Mat *mat, vector<int> &hist, int histSize, const float * range);
void calc_edge_hist( Mat &src_gray, vector<int> &e_hist, float *threshold );
void compare_bgr(int argc, char** argv);
void compare_bgr_bow(int argc, char** argv);
void compare_hsv(int argc, char** argv);
void compare_hsv_bow(int argc, char** argv);
void calc_bgr_bow(int argc, char** argv);
void generate_dictionary(int argc, char **);
void calc_bgr_bow_to_dictionary(int argc, char ** argv);


