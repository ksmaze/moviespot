#include "calc_poster.h"

//////////////////////////////////////////////////////////////////////////

void calc_hist(const Mat *mat, vector<int> &hist, int histSize, const float * range)
{
	hist.assign(histSize, 0);
	uchar *I = mat->data;
	int low = range[0], high = range[1];
	int imsize = mat->cols*mat->rows;
	int block = (high-low)/histSize;
	int tab[257] = {0};
	int i;
	double sum = 0;
	for (i = 0; i < low; i++)
		tab[i] = 0;
	for (i = low; i < high; i++) {
		tab[i] = (i-low)/block;
	}
	for (i = high-1; i < 256; i++)
		tab[i] = histSize-1;
	for (i = 0; i < imsize; i++) {
		hist[tab[(int)I[i]]]++;
		sum++;
	}
	for (i = 0; i < histSize; i++) {
		hist[i] = (int)(((double)hist[i]*100.0)/sum+0.5);
	}
}

void calc_edge_hist( Mat &src_gray, vector<int> &e_hist, float *thresholds )
{
	Mat dst;
	Mat k_v, k_h, k_m45, k_m135, k_nd;
	k_v = Mat::ones(2, 2, CV_32F);
	k_v.at<float>(0,1) = k_v.at<float>(1,1) = -1;
	k_h = Mat::ones(2, 2, CV_32F);
	k_h.at<float>(1,0) = k_h.at<float>(1,1) = -1;
	k_m45 = Mat::zeros(2, 2, CV_32F);
	k_m45.at<float>(0,0) = sqrt(2.0);
	k_m45.at<float>(1,1) = -sqrt(2.0);
	k_m135 = Mat::zeros(2, 2, CV_32F);
	k_m135.at<float>(0,1) = sqrt(2.0);
	k_m135.at<float>(1,0) = -sqrt(2.0);
	k_nd = Mat::ones(2, 2, CV_32F)*2;
	k_nd.at<float>(0,1) = -2;
	k_nd.at<float>(1,0) = -2;
	filter2D(src_gray, dst, CV_32F, k_v);
	threshold(dst, dst, thresholds[0], 1, THRESH_BINARY);
	//imshow("1", src_gray(Rect(j*w_range, i*h_range, w_offset, h_offset)));
	//imshow("2", dst);
	//waitKey();
	e_hist[0] = countNonZero(dst);
	
	filter2D(src_gray, dst, CV_32F, k_h);
	threshold(dst, dst, thresholds[1], 1, THRESH_BINARY);
	//imshow("2", dst);
	//waitKey();
	//threshold(dst, dst, 1, 1, THRESH_BINARY);
	e_hist[1] = countNonZero(dst);
	
	filter2D(src_gray, dst, CV_32F, k_m45);
	threshold(dst, dst, thresholds[2], 1, THRESH_BINARY);
	//imshow("2", dst);
	//waitKey();
	//threshold(dst, dst, 50, 1, THRESH_BINARY);
	e_hist[2] = countNonZero(dst);
	
	filter2D(src_gray, dst, CV_32F, k_m135);
	threshold(dst, dst, thresholds[3], 1, THRESH_BINARY);
	//imshow("2", dst);
	//waitKey();
	//threshold(dst, dst, 50, 1, THRESH_BINARY);
	e_hist[3] = countNonZero(dst);
	
	filter2D(src_gray, dst, CV_32F, k_nd);
	threshold(dst, dst, thresholds[4], 1, THRESH_BINARY);
	//imshow("2", dst);
	//waitKey();
	//threshold(dst, dst, 50, 1, THRESH_BINARY);
	e_hist[4] = countNonZero(dst);
	
}


void compare_bgr(int argc, char** argv)
{
	int i;
	Mat src_base, src_comp;
	src_base = imread(argv[1]);
	src_comp = imread(argv[2]);

	int b_bins = 20, g_bins = 20, r_bins = 20;
	int histSize[] = {b_bins, g_bins, r_bins};

	float b_ranges[] = {0, 256}, g_ranges[] = {0, 256}, r_ranges[] = {0, 256};
	const float* ranges[] = {b_ranges, g_ranges, r_ranges};

	int channels[] = { 0, 1, 2 };

	/// Histograms
	MatND hist_base;
	MatND hist_comp;

	//calcHist(&src_base, 1, channels, Mat(), hist_base, 3, histSize, ranges, true, false);
	//calcHist(&src_comp, 1, channels, Mat(), hist_comp, 3, histSize, ranges, true, false);

	/// Apply the histogram comparison methods
	for( i = 0; i < 4; i++ )
	{ 
		int compare_method = i;
		double base_base = compareHist( hist_base, hist_comp, compare_method );
		printf("%f  ", base_base);
	}

	printf( "\nDone \n" );
}

void compare_bgr_bow(int argc, char** argv)
{
	int i, j,k;
	Mat src_base, src_comp;
	src_base = imread(argv[1]);
	src_comp = imread(argv[2]);
	int h_size = 4, w_size = 4;
	int b_bins = 20, g_bins = 20, r_bins = 20;
	int histSize[] = {b_bins, g_bins, r_bins};

	float b_ranges[] = {0, 256}, g_ranges[] = {0, 256}, r_ranges[] = {0, 256};
	const float* ranges[] = {b_ranges, g_ranges, r_ranges};

	int channels[] = { 0, 1, 2 };

	/// Histograms
	MatND hist_base;
	MatND hist_comp;
	double base_base[4] = {0};
	int h_range = src_base.rows/h_size;
	int w_range = src_base.cols/w_size;
	int h_range2 = src_comp.rows/h_size;
	int w_range2 = src_comp.cols/w_size; 
	for (i = 0; i < h_size; i++){
		for (j = 0; j < w_size; j++){
			int h_offset = (i+1)*h_range<src_base.rows?h_range:src_base.rows-i*h_range;
			int w_offset = (j+1)*w_range<src_base.cols?w_range:src_base.cols-j*w_range;
			int h_offset2 = (i+1)*h_range2<src_comp.rows?h_range2:src_comp.rows-i*h_range2;
			int w_offset2 = (j+1)*w_range2<src_comp.cols?w_range2:src_comp.cols-j*w_range2;
			calcHist(&(src_base(Rect(j*w_range, i*h_range, w_offset, h_offset))), 1, channels, Mat(), hist_base, 3, histSize, ranges, true, false);
			calcHist(&(src_comp(Rect(j*w_range2, i*h_range2, w_offset2, h_offset2))), 1, channels, Mat(), hist_comp, 3, histSize, ranges, true, false);
			imshow("1", src_base(Rect(j*w_range, i*h_range, w_offset, h_offset)));
			imshow("2", src_comp(Rect(j*w_range2, i*h_range2, w_offset2, h_offset2)));
			waitKey();
			for( k = 0; k < 4; k++ )
			{ 
				base_base[k] += compareHist( hist_base, hist_comp, k );
			}
		}
	}

	for( i = 0; i < 4; i++ )
	{ 

		//base_base[i] = compareHist( hist_base, hist_comp, i );
		printf("%f  ", base_base[i]/(h_size*w_size));
	}

	printf( "\nDone \n" );
}

void compare_hsv(int argc, char** argv)
{
	int i;
	Mat src_base, src_comp, hsv_base, hsv_comp;
	src_base = imread(argv[1]);
	src_comp = imread(argv[2]);

	cvtColor( src_base, hsv_base, CV_BGR2HSV );
	cvtColor( src_comp, hsv_comp, CV_BGR2HSV );


	// Using 30 bins for hue and 32 for saturation
	int h_bins = 50; int s_bins = 60;
	int histSize[] = { h_bins, s_bins };


	// hue varies from 0 to 256, saturation from 0 to 180
	float s_ranges[] = { 0, 256 };
	float h_ranges[] = { 0, 180 };

	const float* ranges[] = { h_ranges, s_ranges };


	// Use the o-th and 1-st channels
	int channels[] = { 0, 1 };



	/// Histograms
	MatND hist_base;
	MatND hist_comp;
	/// Calculate the histograms for the HSV images
	calcHist( &hsv_base, 1, channels, Mat(), hist_base, 2, histSize, ranges, true, false );
	normalize( hist_base, hist_base, 0, 1, NORM_MINMAX, -1, Mat() );
	calcHist( &hsv_comp, 1, channels, Mat(), hist_comp, 2, histSize, ranges, true, false );
	normalize( hist_comp, hist_comp, 0, 1, NORM_MINMAX, -1, Mat() );

	/// Apply the histogram comparison methods
	for( i = 0; i < 4; i++ )
	{ 
		int compare_method = i;
		double base_base = compareHist( hist_base, hist_comp, compare_method );
		printf("%f  ", base_base);
	}

	printf( "\nDone \n" );
}

struct eqstr
{
	bool operator()(const char* s1, const char* s2) const
	{
		return strcmp(s1, s2) == 0;
	}
};

hash_map<string, int> visual_dictionary;
typedef pair <string, int> word;

void generate_dictionary(int argc, char **)
{
	char str[120][6] = {0};
	bool flag[5] = {0};
	int count = 0;
	int x1, x2, x3, x4, x5;
	for ( x1 = 0; x1 < 5; x1++)
	{
		if (!flag[x1]) {
			flag[x1] = 1;
			str[count][0] = x1+'0';
		} else
		{
			continue;
		}
		for ( x2 = 0; x2 < 5; x2++)
		{
			if (!flag[x2]) {
				flag[x2] = 1;
				str[count][1] = x2+'0';
			} else
			{
				continue;
			}
			for ( x3 = 0; x3 < 5; x3++)
			{
				if (!flag[x3]) {
					flag[x3] = 1;
					str[count][2] = x3+'0';
				} else
				{
					continue;
				}
				for ( x4 = 0; x4 < 5; x4++)
				{
					if (!flag[x4]) {
						flag[x4] = 1;
						str[count][3] = x4+'0';
					} else
					{
						continue;
					}
					for ( x5 = 0; x5 < 5; x5++)
					{
						if (!flag[x5]) {
							flag[x5] = 1;
							str[count][4] = x5+'0';
							count++;
						} else
						{
							continue;
						}
						flag[x5] = 0;
					}
					flag[x4] = 0;
				}
				flag[x3] = 0;
			}
			flag[x2] = 0;
		}
		flag[x1] = 0;
	}
	count = 0;
	for (x1 = 0; x1 < 120; x1++)
	{
		for (x2 = 0; x2 < 120; x2++)
		{
			for (x3 = 0; x3 < 120; x3++)
			{
				string a(str[x1]);
				a.append(str[x2]);
				a.append(str[x3]);
				visual_dictionary.insert(word(a,count));
				count++;
			}
		}
	}
}

void calc_bgr_bow(int argc, char** argv)
{
	int i, j,k;
	Mat src_base;
	vector<Mat> bgr_planes;
	src_base = imread(argv[1]);

	split( src_base, bgr_planes );
	int h_size = 8, w_size = 6;
	int present[3000] = {0};
	int histSize[] = {5};


	float range[] = {0, 256};
	const float* ranges[] = {range};

	int channels[] = { 0, 1, 2 };

	/// Histograms
	vector<int> b_hist, g_hist, r_hist;
	double base_base[4] = {0};
	int h_range = src_base.rows/h_size;
	int w_range = src_base.cols/w_size;
	for (i = 0; i < h_size; i++){
		for (j = 0; j < w_size; j++){
			int h_offset = (i+1)*h_range<src_base.rows?h_range:src_base.rows-i*h_range;
			int w_offset = (j+1)*w_range<src_base.cols?w_range:src_base.cols-j*w_range;

			calc_hist(&((bgr_planes[0])(Rect(j*w_range, i*h_range, w_offset, h_offset))), b_hist, histSize[0], range);
			calc_hist(&((bgr_planes[1])(Rect(j*w_range, i*h_range, w_offset, h_offset))), g_hist, histSize[0], range);
			calc_hist(&((bgr_planes[2])(Rect(j*w_range, i*h_range, w_offset, h_offset))), r_hist, histSize[0], range);
			//calcHist( &((bgr_planes[0])(Rect(j*w_range, i*h_range, w_offset, h_offset))), 1, 0, Mat(), b_hist, 1, histSize, ranges, true, false );
			//calcHist( &((bgr_planes[1])(Rect(j*w_range, i*h_range, w_offset, h_offset))), 1, 0, Mat(), g_hist, 1, histSize, ranges, true, false );
			//calcHist( &((bgr_planes[2])(Rect(j*w_range, i*h_range, w_offset, h_offset))), 1, 0, Mat(), r_hist, 1, histSize, ranges, true, false );

			//normalize(b_hist, b_hist, 0, 1, NORM_MINMAX, -1, Mat() );
			//normalize(g_hist, g_hist, 0, 1, NORM_MINMAX, -1, Mat() );
			//normalize(r_hist, r_hist, 0, 1, NORM_MINMAX, -1, Mat() );
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*histSize[0]*3+k] = b_hist[k];
			}
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*histSize[0]*3+histSize[0]+k] = g_hist[k];
			}
			for (k = 0; k < histSize[0]; k++)
			{
				present[(i*w_size+j)*histSize[0]*3+histSize[0]*2+k] = r_hist[k];
			}
			printf("%d - %d\n", (i*w_size+j)*histSize[0]*3, (i*w_size+j)*histSize[0]*3+histSize[0]*3-1);
			imshow("1", src_base(Rect(j*w_range, i*h_range, w_offset, h_offset)));
			waitKey();

		}
	}

	for( i = 0; i < h_size*w_size*3*histSize[0]; i++ )
	{ 

		//base_base[i] = compareHist( hist_base, hist_comp, i );
		printf("%d ", present[i]);
		if ((i+1)%(histSize[0]) == 0)
			printf("  ");
		if ((i+1)%(w_size*3*histSize[0]) == 0)
			printf("\n");
	}

	printf( "\nDone \n" );
}

void calc_bgr_bow_to_dictionary(int argc, char ** argv)
{
	Mat src_base, src_comp;
	int h_size = 4, w_size = 4;
	src_base = imread(argv[1]);
	src_comp = imread(argv[2]);

	int b_bins = 5, g_bins = 5, r_bins = 5;
	int histSize[] = {b_bins, g_bins, r_bins};

	float b_ranges[] = {0, 256}, g_ranges[] = {0, 256}, r_ranges[] = {0, 256};
	const float* ranges[] = {b_ranges, g_ranges, r_ranges};

	int channels[] = { 0, 1, 2 };

}
