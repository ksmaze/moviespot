/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.util;

import java.util.Enumeration;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.UpdateableClassifier;
import weka.core.AdditionalMeasureProducer;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.OptionHandler;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformationHandler;
import weka.core.WeightedInstancesHandler;

/**
 *
 * @author KsMaze
 */
public class ensembleLDDM extends AbstractClassifier
    implements OptionHandler, UpdateableClassifier, WeightedInstancesHandler,
        TechnicalInformationHandler, AdditionalMeasureProducer{
    
    LDDM[] m_Classifiers = null;

    //by aYa
    //build every LDDM
    @Override
    public void buildClassifier(Instances data) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet.");
        m_Classifiers = new LDDM[data.size()];
        for (int i=0; i<data.size(); i++)	{
        	m_Classifiers[i] = new LDDM();
                
        	m_Classifiers[i].buildClassifier(data,i); 
        }
    }

    //by aYa
    //classify using every LDDM
    @Override
    public double classifyInstance(Instance instance) throws Exception {
    	double predictionValue = 0;
    	int nonLocal = 0;
    	
//    	for (int i=0; i<m_Classifiers.length; i++)	{
//    		//if (m_Classifiers[i].classifyInstance(instance)!= -1)	
//    		//	predictionValue += m_Classifiers[i].classifyInstance(instance);
//    		//else
//    		//	nonLocal ++;
//            
//    	}
        
        double[] dis = distributionForInstance(instance);
        for (int i = 0; i < dis.length; i++) {
            predictionValue += dis[i]*i;
        }
    	
//    	predictionValue /= (m_Classifiers.length-nonLocal);
    	
        return predictionValue;
    }
    
    //by aYa
    //classify using every LDDM, combined the distribution
    @Override
    public double[] distributionForInstance(Instance instance) throws Exception {
    	double predictionValue = 0;
    	double[] probabilityDistribution = new double[11];
    	int Local = 0;
    	int i;
    	
    	for (i=0; i<probabilityDistribution.length; i++){
    		probabilityDistribution[i] = 0;
    	}
    	
    	for (i=0; i<m_Classifiers.length; i++)	{
            double[] dis = m_Classifiers[i].distributionForInstance(instance);
            if (dis[0] > -1) {
                for (int j = 0; j < dis.length; j++) {
                    probabilityDistribution[j] += dis[j];
                }
                Local++;
            }
    	}
    	
    	for (i=0; i<probabilityDistribution.length; i++){
    		probabilityDistribution[i] /= Local;
    	}
    	
        return probabilityDistribution;
    }

    //following you don't have to implement
    @Override
    public void updateClassifier(Instance instance) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public TechnicalInformation getTechnicalInformation() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Enumeration enumerateMeasures() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getMeasure(String measureName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
