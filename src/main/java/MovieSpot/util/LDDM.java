/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.UpdateableClassifier;
import weka.classifiers.lazy.IBk;
import weka.core.AdditionalMeasureProducer;
import weka.core.Attribute;
import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.OptionHandler;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformationHandler;
import weka.core.WeightedInstancesHandler;
import weka.core.matrix.EigenvalueDecomposition;
import weka.core.matrix.Matrix;
import weka.core.neighboursearch.LinearNNSearch;
import weka.core.neighboursearch.NearestNeighbourSearch;

/**
 *
 * @author Administrator
 */
public class LDDM extends AbstractClassifier
        implements OptionHandler, UpdateableClassifier, WeightedInstancesHandler,
        TechnicalInformationHandler, AdditionalMeasureProducer {

    protected int m_k1;
    protected int m_k2;
    protected int m_K;
    protected int m_dd;
    protected double m_beta;
    protected double max_distance;
    protected Matrix m_W;
    protected Instances m_Train;
    protected Instances neighbors;
    protected int m_NumClasses;
    
    protected int m_ClassType;
    
    protected int m_kNN;
    
    protected int m_kNNUpper;
    
    
    
    protected CustomizeDistance m_DistanceFunction = new CustomizeDistance();
    protected WeightedDistance m_WeightedDistance = new WeightedDistance();
    
    protected ClassLinearNNSearch m_NNSearch = new ClassLinearNNSearch();

    public LDDM() throws Exception {
        m_k1 = 200;
        m_k2 = 200;
        m_K = 5;
        init();
    }
    
    protected void init() throws Exception {
        m_NNSearch.setDistanceFunction(m_DistanceFunction);
    }

    public void buildClassifier(Instances instances, int idx) throws Exception {
        Instances focal_vicinity = new Instances(instances, m_k1 + m_k2 + 1);
        Instance focal_sample = (Instance)instances.instance(idx).copy();
        
        m_Train = instances;
        m_NNSearch.setInstances(m_Train);
        focal_vicinity.add(focal_sample);
        Instances same_class = m_NNSearch.kNearestNeighbours(focal_sample, true, m_k1);
        m_k1 = same_class.size();
        for (int i = 0; i < m_k1; i++) {
            focal_vicinity.add(same_class.instance(i));
        }
        Instances different_class = m_NNSearch.kNearestNeighbours(focal_sample, false, m_k2);
        m_k2 = different_class.size();
        for (int i = 0; i < m_k2; i++) {
            focal_vicinity.add(different_class.instance(i));
        }
        //buildClassifier(focal_vicinity);
        
        //instances.insertAttributeAt(new Attribute("distance"), focal_vicinity.numAttributes());
        //m_NNSearch.setInstances(focal_vicinity);
        //for (int i = 1; i < focal_vicinity.size(); i++) {
        //    focal_vicinity.instance(i).setValue(focal_vicinity.numAttributes()-1, m_DistanceFunction.distance(focal_sample, instances.instance(i)));
        //}
        //instances.sort(instances.numAttributes()-1);
        //instances.deleteAttributeAt(instances.numAttributes()-1);
        m_k1 = (instances.size()-1)/2;
        m_k2 = instances.size()-1-m_k1;
        //build Xi*Li*Xi' (d*d)
        Matrix matrix = new Matrix(instances.numAttributes(), instances.numAttributes());
        for (int i = 0; i < instances.numAttributes(); i++) {
            for (int j = 0; j < instances.numAttributes(); j++) {
                double value = 0.0;
                for (int k = 0; k < m_k1; k++) {
                    value += m_DistanceFunction.difference(instances.instance(1), instances.instance(1+k), i) *
                            m_DistanceFunction.difference(instances.instance(1), instances.instance(1+k), j);
                }
                for (int k = 0; k < m_k2; k++) {
                    value -= m_beta * m_DistanceFunction.difference(instances.instance(1), instances.instance(1+m_k1+k), i) *
                            m_DistanceFunction.difference(instances.instance(1), instances.instance(1+m_k1+k), j);
                }
                matrix.set(i, j, value);
            }
        }
        EigenvalueDecomposition eigen = new EigenvalueDecomposition(matrix);
        //m_W = eigen.getV();
        Matrix mat = eigen.getV();
        double[] array = eigen.getRealEigenvalues();
        ArrayIndexComparator comparator = new ArrayIndexComparator(array);
        Integer[] indexes = comparator.createIndexArray();
        Arrays.sort(indexes, comparator);
        
        //should select d' for dimension
        m_dd = array.length/2;
        m_W = new Matrix(m_dd, array.length);
        for (int i = 0; i < m_dd; i++) {
            for (int j = 0; j < array.length; j++) {
                m_W.set(i, j, mat.get(indexes[i], j));
            }
        }
        //find K nearest 
        m_WeightedDistance.setM_W(m_W);
        m_NNSearch.setDistanceFunction(m_WeightedDistance);
        m_NNSearch.setInstances(instances);
        neighbors = m_NNSearch.kNearestNeighbours(instances.firstInstance(), m_K);
        
        max_distance = 0;
        for (Instance instance : neighbors) {
            double distance = Distance(instances.firstInstance(),instance);
            if (distance > max_distance) 
                max_distance = distance;
        }
        neighbors.add(instances.firstInstance());
    }

    @Override
    public void buildClassifier(Instances instances) throws Exception {
//        Instance focal_sample = instances.firstInstance();
//        instances.insertAttributeAt(new Attribute("distance"), instances.numAttributes());
//        m_NNSearch.setInstances(instances);
//        for (int i = 1; i < instances.size(); i++) {
//            instances.instance(i).setValue(instances.numAttributes()-1, m_DistanceFunction.distance(focal_sample, instances.instance(i)));
//        }
//        //instances.sort(instances.numAttributes()-1);
//        //instances.deleteAttributeAt(instances.numAttributes()-1);
//        //m_k1 = (instances.size()-1)/2;
//        //m_k2 = instances.size()-1-m_k1;
//        //build Xi*Li*Xi' (d*d)
//        Matrix matrix = new Matrix(instances.numAttributes(), instances.numAttributes());
//        for (int i = 0; i < instances.numAttributes(); i++) {
//            for (int j = 0; j < instances.numAttributes(); j++) {
//                double value = 0.0;
//                for (int k = 0; k < m_k1; k++) {
//                    value += m_DistanceFunction.difference(instances.instance(1), instances.instance(k), i) *
//                            m_DistanceFunction.difference(instances.instance(1), instances.instance(k), j);
//                }
//                for (int k = 0; k < m_k2; k++) {
//                    value -= m_beta * m_DistanceFunction.difference(instances.instance(1), instances.instance(k), i) *
//                            m_DistanceFunction.difference(instances.instance(1), instances.instance(k), j);
//                }
//                matrix.set(i, j, value);
//            }
//        }
//        EigenvalueDecomposition eigen = new EigenvalueDecomposition(matrix);
//        //m_W = eigen.getV();
//        Matrix mat = eigen.getV();
//        double[] array = eigen.getRealEigenvalues();
//        ArrayIndexComparator comparator = new ArrayIndexComparator(array);
//        Integer[] indexes = comparator.createIndexArray();
//        Arrays.sort(indexes, comparator);
//        
//        //should select d' for dimension
//        m_dd = array.length/2;
//        m_W = new Matrix(m_dd, array.length);
//        for (int i = 0; i < m_dd; i++) {
//            for (int j = 0; j < array.length; j++) {
//                m_W.set(i, j, mat.get(indexes[i], j));
//            }
//        }
//        //find K nearest 
//        m_WeightedDistance.setM_W(m_W);
//        m_NNSearch.setDistanceFunction(m_WeightedDistance);
//        m_NNSearch.setInstances(instances);
//        neighbors = m_NNSearch.kNearestNeighbours(instances.firstInstance(), m_K);
//        
//        max_distance = 0;
//        for (Instance instance : neighbors) {
//            double distance = Distance(instances.firstInstance(),instance);
//            if (distance > max_distance) 
//                max_distance = distance;
//        }
//        neighbors.add(instances.firstInstance());
    }

    private double Distance(Instance first, Instance second) {
        
        double[] diff = m_DistanceFunction.difference(first, second);
        double dis = 0.0;
        for (int i = 0; i < m_dd; i++) {
            double temp = 0;
            for (int j = 0; j < diff.length; j++) {
                temp += diff[j]*m_W.get(i, j);
            }
            dis += temp*temp;
        }
        dis = Math.sqrt(dis);
        return dis;
    }
    
    private class ArrayIndexComparator implements Comparator<Integer>
    {
        private final double[] array;

        public ArrayIndexComparator(double[] array)
        {
            this.array = array;
        }

        public Integer[] createIndexArray()
        {
            Integer[] indexes = new Integer[array.length];
            for (int i = 0; i < array.length; i++)
            {
                indexes[i] = i; // Autoboxing
            }
            return indexes;
        }

        @Override
        public int compare(Integer index1, Integer index2)
        {
             // Autounbox from Integer to int to use as array indexes
            return Double.compare(array[index1], array[index2]);
        }
    }

    //By KsMaze
    @Override
    public double[] distributionForInstance(Instance instance) throws Exception {
        return super.distributionForInstance(instance);
//        double sum = 0.0;
//        double[] num = new double[11];
//        for (Instance instance1 : neighbors) {
//            if (Distance(instance, instance1) < max_distance) {
//                num[Math.round((float) instance1.classValue())]++;
//                sum++;
//            }
//        }
//        for (int i = 0; i <= 10; i++) {
//            num[i] /= sum;
//        }
//        return num;
    }

    //By KsMaze
    protected double[] makeDistribution(Instances neighbours, double[] distances) throws Exception {
        return null;
    }

    //By KsMaze
    @Override
    public double classifyInstance(Instance instance) throws Exception {
        double ans = 0.0;
        double num = 0.0;
        for (Instance instance1 : neighbors) {
            if (Distance(instance, instance1) < max_distance) {
                num+=1;
                ans+=instance1.classValue();
            }
        }
        if (num > 0) 
            ans /= num;
        return ans;
    }

    /**
     * Returns a string describing classifier.
     *
     * @return a description suitable for displaying in the
     * explorer/experimenter gui
     */
    public String globalInfo() {

        return "Local Discriminative Distance Metrics classifier, will be combined with Ensemble Learning.\n\n"
                + getTechnicalInformation().toString();
    }

    @Override
    public void updateClassifier(Instance instance) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public TechnicalInformation getTechnicalInformation() {
        TechnicalInformation result;

        result = new TechnicalInformation(TechnicalInformation.Type.ARTICLE);
        result.setValue(TechnicalInformation.Field.AUTHOR, "Yang Mu");
        result.setValue(TechnicalInformation.Field.YEAR, "2013");
        result.setValue(TechnicalInformation.Field.TITLE, "Local Discriminative Distance Metrics Ensemble Learning");

        return result;
    }

    @Override
    public Enumeration enumerateMeasures() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getMeasure(String measureName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Matrix getM_W() {
        return m_W;
    }
    
    
}
