/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class testSub {
    
    public static void main(String[] args) throws FileNotFoundException {
        FileWriter writer = null;
        try {
            String csv1 = "id_poster.csv";
            String csv2 = "id_social.csv";
            String ans = "ans.csv";
            Scanner reader1 = new Scanner(new File(csv1));
            reader1.useDelimiter("\r\n");
            Scanner reader2 = new Scanner(new File(csv2));
            reader2.useDelimiter("\r\n");
            writer = new FileWriter(ans);
            while(reader1.hasNext()) {
                String[] items1 = reader1.next().split(",");
                String[] items2 = reader2.next().split(",");
                if (items1[0].equals(items2[0])) {
                    StringBuilder temp = new StringBuilder();
                    temp.append(items1[0]).append(",");
                    Set<String> s1 = new HashSet<>(5);
                    Set<String> s2 = new HashSet<>(5);
                    for (int i = 1; i < items1.length; i++) {
                        s1.add(items1[i]);
                    }
                    for (int i = 1; i < items2.length; i++) {
                        s2.add(items2[i]);
                    }
                    s1.retainAll(s2);
                    temp.append((double)s1.size()/5).append("\r\n");
                    writer.append(temp.toString());
                }
            }
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(testSub.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(testSub.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
}
