/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.util;

import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.neighboursearch.LinearNNSearch;

/**
 *
 * @author Administrator
 */
public class ClassLinearNNSearch extends LinearNNSearch {

    
    
    @Override
    public void setInstances(Instances insts) throws Exception {
        super.setInstances(insts);
    }

    @Override
    public void setDistanceFunction(DistanceFunction df) throws Exception {
        super.setDistanceFunction(df);
    }

    
    
    public Instance nearestNeighbour(Instance target, int m_class) throws Exception {

        return super.nearestNeighbour(target);
    }

    @Override
    public Instance nearestNeighbour(Instance target) throws Exception {
        return super.nearestNeighbour(target);
    }

    public Instances kNearestNeighbours(Instance target, boolean same_class, int kNN) throws Exception {

        if (m_Stats != null) {
            m_Stats.searchStart();
        }

        MyHeap heap = new MyHeap(kNN);
        double distance;
        int firstkNN = 0;
        for (int i = 0; i < m_Instances.numInstances(); i++) {
            if (target == m_Instances.instance(i)) //for hold-one-out cross-validation
            {
                continue;
            }
            if (m_Stats != null) {
                m_Stats.incrPointCount();
            }
            if ((same_class && Math.abs(m_Instances.instance(i).classValue()-target.classValue()) < 1.0)
                    || (!same_class && Math.abs(m_Instances.instance(i).classValue()-target.classValue()) >= 1.0)) {
                if (firstkNN < kNN) {
                    distance = m_DistanceFunction.distance(target, m_Instances.instance(i), Double.POSITIVE_INFINITY, m_Stats);
                    if (distance == 0.0 && m_SkipIdentical) {
                        if (i < m_Instances.numInstances() - 1) {
                            continue;
                        } else {
                            heap.put(i, distance);
                        }
                    }
                    heap.put(i, distance);
                    firstkNN++;
                } else {
                    MyHeapElement temp = heap.peek();
                    distance = m_DistanceFunction.distance(target, m_Instances.instance(i), temp.distance, m_Stats);
                    if (distance == 0.0 && m_SkipIdentical) {
                        continue;
                    }
                    if (distance < temp.distance) {
                        heap.putBySubstitute(i, distance);
                    } else if (distance == temp.distance) {
                        heap.putKthNearest(i, distance);
                    }
                }
            }
        }

        Instances neighbours = new Instances(m_Instances, (heap.size() + heap.noOfKthNearest()));
        m_Distances = new double[heap.size() + heap.noOfKthNearest()];
        int[] indices = new int[heap.size() + heap.noOfKthNearest()];
        int i = 1;
        MyHeapElement h;

        //m_DistanceFunction.postProcessDistances(m_Distances);
        
        while(heap.noOfKthNearest()>0) {
            h = heap.getKthNearest();
            neighbours.add(m_Instances.instance(h.index));
        }
        while(heap.size()>0) {
            h = heap.get();
            neighbours.add(m_Instances.instance(h.index));
        }

        if (m_Stats != null) {
            m_Stats.searchFinish();
        }

        return neighbours;
    }

    @Override
    public Instances kNearestNeighbours(Instance target, int kNN) throws Exception {
        return super.kNearestNeighbours(target, kNN);
    }
}
