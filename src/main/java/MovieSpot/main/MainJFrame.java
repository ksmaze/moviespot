/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.main;

import MovieSpot.core.Movie;
import MovieSpot.core.MovieRecommender;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;

/**
 *
 * @author Administrator
 */
public class MainJFrame extends javax.swing.JFrame {

    static MovieRecommender recommender;
    static javax.swing.DefaultListModel listMovieModel;
    static long currUserId;
    static ArrayList<Movie> userHistory;
    
    /**
     * Creates new form MainJFrame
     */
    
    private void setDisplay(int idx) {
        Movie selectedMovie = userHistory.get(idx);

        ImageIcon poster = new ImageIcon("./res/" + Integer.toString(selectedMovie.getIndex()) + ".jpg");
        poster.setImage(poster.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        posterLabel.setIcon(poster);

        String movieName = selectedMovie.getName();
        if (movieName.length() > 40)
            movieName = movieName.substring(0, 36) + "...";
        titleLabel.setText(movieName);


        String genreName = selectedMovie.getGenreNames();

        if (genreName.length() > 40)
            genreName = genreName.substring(0, 36) + "...";
        genreLabel.setText(genreName);
        releaseLabel.setText(Integer.toString(selectedMovie.getRelease_year()));
        directorLabel.setText(selectedMovie.getDirectorName());
        writerLabel.setText(selectedMovie.getWriterName());
        ratingLabel.setText(selectedMovie.getRatingName());
        imdbRatingLabel.setText(Double.toString(selectedMovie.getRate()));
   
        starringLabel.setText(selectedMovie.getStarNames());

        ArrayList<Movie> contentResult = recommender.recommendbasedContent(selectedMovie, 5);
        ArrayList<Movie> socialResult = recommender.recommendbasedSocial(selectedMovie, 5);

        ArrayList<String> contentName = new ArrayList<>();
        for (Movie tmp:contentResult) {
            if (tmp.getName().length() > 23)
                contentName.add(tmp.getName().substring(0, 22) + "...");
            else
                contentName.add(tmp.getName());
        }
        ArrayList<String> socialName = new ArrayList<>();
        for (Movie tmp:socialResult) {
            if (tmp.getName().length() > 25)
                socialName.add(tmp.getName().substring(0, 22) + "...");
            else
                socialName.add(tmp.getName());
        }

        socialTitle1.setText(socialName.get(0));
        socialTitle2.setText(socialName.get(1));
        socialTitle3.setText(socialName.get(2));
        socialTitle4.setText(socialName.get(3));
        socialTitle5.setText(socialName.get(4));

        contentTitle1.setText(contentName.get(0));
        contentTitle2.setText(contentName.get(1));
        contentTitle3.setText(contentName.get(2));
        contentTitle4.setText(contentName.get(3));
        contentTitle5.setText(contentName.get(4));

        ImageIcon sposter1 = new ImageIcon("./res/" + Integer.toString( socialResult.get(0).getIndex() ) + ".jpg");
        sposter1.setImage(sposter1.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        socialPoster1.setIcon(sposter1);
        ImageIcon sposter2 = new ImageIcon("./res/" + Integer.toString( socialResult.get(1).getIndex() ) + ".jpg");
        sposter2.setImage(sposter2.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        socialPoster2.setIcon(sposter2);
        ImageIcon sposter3 = new ImageIcon("./res/" + Integer.toString( socialResult.get(2).getIndex() ) + ".jpg");
        sposter3.setImage(sposter3.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        socialPoster3.setIcon(sposter3);
        ImageIcon sposter4 = new ImageIcon("./res/" + Integer.toString( socialResult.get(3).getIndex() ) + ".jpg");
        sposter4.setImage(sposter4.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        socialPoster4.setIcon(sposter4);
        ImageIcon sposter5 = new ImageIcon("./res/" + Integer.toString( socialResult.get(4).getIndex() ) + ".jpg");
        sposter5.setImage(sposter5.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        socialPoster5.setIcon(sposter5);

        ImageIcon cposter1 = new ImageIcon("./res/" + Integer.toString( contentResult.get(0).getIndex() ) + ".jpg");
        cposter1.setImage(cposter1.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        contentPoster1.setIcon(cposter1);
        ImageIcon cposter2 = new ImageIcon("./res/" + Integer.toString( contentResult.get(1).getIndex() ) + ".jpg");
        cposter2.setImage(cposter2.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        contentPoster2.setIcon(cposter2);
        ImageIcon cposter3 = new ImageIcon("./res/" + Integer.toString( contentResult.get(2).getIndex() ) + ".jpg");
        cposter3.setImage(cposter3.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        contentPoster3.setIcon(cposter3);
        ImageIcon cposter4 = new ImageIcon("./res/" + Integer.toString( contentResult.get(3).getIndex() ) + ".jpg");
        cposter4.setImage(cposter4.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        contentPoster4.setIcon(cposter4);
        ImageIcon cposter5 = new ImageIcon("./res/" + Integer.toString( contentResult.get(4).getIndex() ) + ".jpg");
        cposter5.setImage(cposter5.getImage().getScaledInstance(157, 219, Image.SCALE_DEFAULT));
        contentPoster5.setIcon(cposter5);
    }
    
    public MainJFrame() {
        recommender = new MovieRecommender(true);
        recommender.build();
        initComponents();
        
        currUserId = 235;
        userIdLabel.setText((""+currUserId));
        
        // add already seen movie
        userHistory = recommender.getUserHistory(currUserId);
        for (Movie tmp:userHistory) {
            String title = tmp.getName() + " ()";
            listMovieModel.addElement(title);
        }
        
        // display data
        setDisplay(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        historyMovieList = new javax.swing.JList();
        jlabel9 = new javax.swing.JLabel();
        alrdyseenLabel = new javax.swing.JLabel();
        posterLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        titleLabel = new javax.swing.JLabel();
        releaseLabel = new javax.swing.JLabel();
        genreLabel = new javax.swing.JLabel();
        directorLabel = new javax.swing.JLabel();
        starringLabel = new javax.swing.JLabel();
        userIdLabel = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        socialTitle1 = new javax.swing.JLabel();
        socialPoster1 = new javax.swing.JLabel();
        socialPoster2 = new javax.swing.JLabel();
        socialTitle2 = new javax.swing.JLabel();
        socialPoster3 = new javax.swing.JLabel();
        socialTitle3 = new javax.swing.JLabel();
        socialPoster4 = new javax.swing.JLabel();
        socialTitle4 = new javax.swing.JLabel();
        contentTitle5 = new javax.swing.JLabel();
        contentTitle4 = new javax.swing.JLabel();
        contentPoster5 = new javax.swing.JLabel();
        contentPoster2 = new javax.swing.JLabel();
        contentTitle1 = new javax.swing.JLabel();
        contentPoster1 = new javax.swing.JLabel();
        contentPoster3 = new javax.swing.JLabel();
        contentTitle3 = new javax.swing.JLabel();
        contentPoster4 = new javax.swing.JLabel();
        contentTitle2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        ratingLabel = new javax.swing.JLabel();
        imdbRatingLabel = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        writerLabel = new javax.swing.JLabel();
        socialPoster5 = new javax.swing.JLabel();
        socialTitle5 = new javax.swing.JLabel();
        alrdyseenLabel1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MovieSpot");
        setBounds(new java.awt.Rectangle(0, 0, 920, 934));
        setPreferredSize(new java.awt.Dimension(920, 1000));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        listMovieModel = new javax.swing.DefaultListModel();
        historyMovieList.setModel(listMovieModel);
        historyMovieList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                historyMovieListMouseClicked(evt);
            }
        });
        historyMovieList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                historyMovieListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(historyMovieList);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 82, 314, 212));

        jlabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jlabel9.setLabelFor(userIdLabel);
        jlabel9.setText("User ID");
        getContentPane().add(jlabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 26, -1, -1));
        jlabel9.getAccessibleContext().setAccessibleName("uid");
        jlabel9.getAccessibleContext().setAccessibleDescription("");

        alrdyseenLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        alrdyseenLabel.setLabelFor(historyMovieList);
        alrdyseenLabel.setText("About...");
        getContentPane().add(alrdyseenLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 60, -1, -1));

        posterLabel.setIcon(null);
        getContentPane().add(posterLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(374, 82, 157, 212));

        jLabel1.setLabelFor(titleLabel);
        jLabel1.setText("TITLE");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 82, -1, -1));

        jLabel2.setLabelFor(releaseLabel);
        jLabel2.setText("RELEASE YEAR");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 107, -1, -1));

        jLabel3.setLabelFor(genreLabel);
        jLabel3.setText("GENRE");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 270, -1, -1));

        jLabel4.setLabelFor(directorLabel);
        jLabel4.setText("DIRECTOR");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 157, -1, -1));

        jLabel5.setLabelFor(starringLabel);
        jLabel5.setText("STARRING");
        jLabel5.setToolTipText("");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 210, -1, -1));
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(223, 61, -1, -1));
        jLabel7.getAccessibleContext().setAccessibleName("titleLabel");

        titleLabel.setText("jLabel8");
        getContentPane().add(titleLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(668, 82, -1, -1));

        releaseLabel.setText("jLabel8");
        getContentPane().add(releaseLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(668, 107, -1, -1));

        genreLabel.setText("jLabel8");
        getContentPane().add(genreLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 270, -1, -1));

        directorLabel.setText("jLabel8");
        getContentPane().add(directorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(668, 157, -1, -1));

        starringLabel.setText("jLabel8");
        getContentPane().add(starringLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(668, 210, -1, -1));

        userIdLabel.setText("jLabel8");
        getContentPane().add(userIdLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(126, 29, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("People who liked this also liked... ");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 324, -1, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Similar movies that you might like...");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 633, -1, -1));

        socialTitle1.setText("recommended movie");
        getContentPane().add(socialTitle1, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 595, -1, -1));

        socialPoster1.setText("jLabel11");
        getContentPane().add(socialPoster1, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 352, 157, 219));

        socialPoster2.setText("jLabel11");
        getContentPane().add(socialPoster2, new org.netbeans.lib.awtextra.AbsoluteConstraints(207, 352, 157, 219));

        socialTitle2.setText("recommended movie");
        getContentPane().add(socialTitle2, new org.netbeans.lib.awtextra.AbsoluteConstraints(207, 595, -1, -1));

        socialPoster3.setText("jLabel11");
        getContentPane().add(socialPoster3, new org.netbeans.lib.awtextra.AbsoluteConstraints(382, 352, 157, 219));

        socialTitle3.setText("recommended movie");
        getContentPane().add(socialTitle3, new org.netbeans.lib.awtextra.AbsoluteConstraints(382, 595, -1, -1));

        socialPoster4.setText("jLabel11");
        getContentPane().add(socialPoster4, new org.netbeans.lib.awtextra.AbsoluteConstraints(557, 352, 157, 219));

        socialTitle4.setText("recommended movie");
        getContentPane().add(socialTitle4, new org.netbeans.lib.awtextra.AbsoluteConstraints(557, 595, -1, -1));

        contentTitle5.setText("recommended movie");
        getContentPane().add(contentTitle5, new org.netbeans.lib.awtextra.AbsoluteConstraints(732, 911, -1, -1));

        contentTitle4.setText("recommended movie");
        getContentPane().add(contentTitle4, new org.netbeans.lib.awtextra.AbsoluteConstraints(557, 911, -1, -1));

        contentPoster5.setText("jLabel11");
        getContentPane().add(contentPoster5, new org.netbeans.lib.awtextra.AbsoluteConstraints(732, 668, 157, 219));

        contentPoster2.setText("jLabel11");
        getContentPane().add(contentPoster2, new org.netbeans.lib.awtextra.AbsoluteConstraints(207, 668, 157, 219));

        contentTitle1.setText("recommended movie");
        getContentPane().add(contentTitle1, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 911, -1, -1));

        contentPoster1.setText("jLabel11");
        getContentPane().add(contentPoster1, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 668, 157, 219));

        contentPoster3.setText("jLabel11");
        getContentPane().add(contentPoster3, new org.netbeans.lib.awtextra.AbsoluteConstraints(382, 668, 157, 219));

        contentTitle3.setText("recommended movie");
        getContentPane().add(contentTitle3, new org.netbeans.lib.awtextra.AbsoluteConstraints(382, 911, -1, -1));

        contentPoster4.setText("jLabel11");
        getContentPane().add(contentPoster4, new org.netbeans.lib.awtextra.AbsoluteConstraints(557, 668, 157, 219));

        contentTitle2.setText("recommended movie");
        getContentPane().add(contentTitle2, new org.netbeans.lib.awtextra.AbsoluteConstraints(207, 911, -1, -1));

        jLabel6.setLabelFor(starringLabel);
        jLabel6.setText("RATING");
        jLabel6.setToolTipText("");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 132, -1, -1));

        jLabel10.setLabelFor(starringLabel);
        jLabel10.setText("IMDb RATING");
        jLabel10.setToolTipText("");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 290, -1, -1));

        ratingLabel.setText("jLabel8");
        getContentPane().add(ratingLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(668, 132, -1, -1));

        imdbRatingLabel.setText("jLabel8");
        getContentPane().add(imdbRatingLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 290, -1, -1));

        jLabel11.setLabelFor(starringLabel);
        jLabel11.setText("WRITER");
        jLabel11.setToolTipText("");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 182, -1, -1));

        writerLabel.setText("jLabel8");
        getContentPane().add(writerLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(668, 182, -1, -1));

        socialPoster5.setText("jLabel11");
        getContentPane().add(socialPoster5, new org.netbeans.lib.awtextra.AbsoluteConstraints(732, 352, 157, 219));

        socialTitle5.setText("recommended movie");
        getContentPane().add(socialTitle5, new org.netbeans.lib.awtextra.AbsoluteConstraints(732, 595, -1, -1));

        alrdyseenLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        alrdyseenLabel1.setLabelFor(historyMovieList);
        alrdyseenLabel1.setText("History...");
        getContentPane().add(alrdyseenLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 59, -1, -1));

        jButton2.setText("Logout");
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 20, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void historyMovieListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_historyMovieListMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_historyMovieListMouseClicked

    private void historyMovieListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_historyMovieListValueChanged
        int idx = historyMovieList.getSelectedIndex();
        if (idx != -1) {
            setDisplay(idx);
        }
    }//GEN-LAST:event_historyMovieListValueChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel alrdyseenLabel;
    private javax.swing.JLabel alrdyseenLabel1;
    private javax.swing.JLabel contentPoster1;
    private javax.swing.JLabel contentPoster2;
    private javax.swing.JLabel contentPoster3;
    private javax.swing.JLabel contentPoster4;
    private javax.swing.JLabel contentPoster5;
    private javax.swing.JLabel contentTitle1;
    private javax.swing.JLabel contentTitle2;
    private javax.swing.JLabel contentTitle3;
    private javax.swing.JLabel contentTitle4;
    private javax.swing.JLabel contentTitle5;
    private javax.swing.JLabel directorLabel;
    private javax.swing.JLabel genreLabel;
    private javax.swing.JList historyMovieList;
    private javax.swing.JLabel imdbRatingLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jlabel9;
    private javax.swing.JLabel posterLabel;
    private javax.swing.JLabel ratingLabel;
    private javax.swing.JLabel releaseLabel;
    private javax.swing.JLabel socialPoster1;
    private javax.swing.JLabel socialPoster2;
    private javax.swing.JLabel socialPoster3;
    private javax.swing.JLabel socialPoster4;
    private javax.swing.JLabel socialPoster5;
    private javax.swing.JLabel socialTitle1;
    private javax.swing.JLabel socialTitle2;
    private javax.swing.JLabel socialTitle3;
    private javax.swing.JLabel socialTitle4;
    private javax.swing.JLabel socialTitle5;
    private javax.swing.JLabel starringLabel;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JLabel userIdLabel;
    private javax.swing.JLabel writerLabel;
    // End of variables declaration//GEN-END:variables
}
