package MovieSpot.main;

import MovieSpot.core.Movie;
import MovieSpot.core.MovieRecommender;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class App {
    
    static MovieRecommender recommender;
    
    
    public static void saveAllRecommendtoCSV() throws Exception {
        FileWriter writer = new FileWriter("result-prd.csv", false);
        writer.write("id,name,recommend1,recommend2,recommend3,recommend4,recommend5\r\n");
        ArrayList<Movie> movies = recommender.getMovies();
        for (Movie movie : movies) {
            StringBuilder temp = new StringBuilder();
            temp.append(movie.getIndex()).append(",");
            temp.append("\"").append(movie.getName()).append("\"").append(",");
            ArrayList<Movie> results = recommender.recommendbasedContent(movie, 5);
            for (int i = 0; i < results.size()-1; i ++) {
                temp.append("\"").append(results.get(i).getName()).append("\"").append(",");
            }
            temp.append("\"").append(results.get(results.size()-1).getName()).append("\"");
            temp.append("\r\n");
            writer.write(temp.toString());
        }
        writer.flush();
        writer.close();
    }
    
    public static void saveAverageRate() throws Exception {
        FileWriter writer = new FileWriter("result-prd.csv", false);
        writer.write("id,name,average\r\n");
        ArrayList<Movie> movies = recommender.getMovies();
        for (Movie movie : movies) {
            StringBuilder temp = new StringBuilder();
            temp.append(movie.getIndex()).append(",");
            temp.append("\"").append(movie.getName()).append("\"").append(",");
            ArrayList<Movie> results = recommender.recommendbasedContent(movie, 5);
            double average = 0.0;
            for (int i = 0; i < results.size(); i ++) {
                average += results.get(i).getRate();
            }
            temp.append("\"").append(average/results.size()).append("\"");
            temp.append("\r\n");
            writer.write(temp.toString());
        }
        writer.flush();
        writer.close();
    }
    
    public static void testCoverage() throws Exception {
        FileWriter writer = new FileWriter("result-coverage.csv", false);
        writer.write("id,coverage\r\n");
        String csv1 = "u.data";
        Scanner reader1 = new Scanner(new File(csv1));
        reader1.useDelimiter("\n");
        HashMap<Integer, ArrayList<Integer>> user = new HashMap<>();
        while(reader1.hasNext()) {
            String[] items1 = reader1.next().split("\t");
            if (user.containsKey(Integer.valueOf(items1[0]))) {
                user.get(Integer.valueOf(items1[0])).add(Integer.valueOf(items1[1]));
            } else {
                user.put(Integer.valueOf(items1[0]), new ArrayList<Integer>());
                user.get(Integer.valueOf(items1[0])).add(Integer.valueOf(items1[1]));
            }
        }
        

        for (Map.Entry<Integer, ArrayList<Integer>> entry : user.entrySet()) {
            try {
                Integer integer = entry.getKey();
                ArrayList<Integer> arrayList = entry.getValue();
                ArrayList<Movie> movies = recommender.recommendbasedSocialforUser(integer, 5);
                int length = movies.size();
                Set<Integer> s1 = new HashSet<>();
                for (int j = 0; j < movies.size(); j++) {
                    s1.add(movies.get(j).getIndex());
                }
                
                int len = arrayList.size();
                //len = len>10?10:len;
                for (int j = 0; j < len; j++) {
                    ArrayList<Movie> test = recommender.recommendbasedContent(recommender.findMovie(arrayList.get(j)), 5);
                    for (Movie movie : test) {
                        s1.remove(movie.getIndex());
                    }
                }
                writer.write(String.valueOf(integer)+","+String.valueOf((double)(length-s1.size())/length)+"\r\n");
                writer.flush();
            } catch (Exception e) {
            }
            
        }
        
        writer.flush();
        writer.close();
    }
    
    public static void main(String[] args) {
        recommender = new MovieRecommender(true);
        recommender.build();
        try {
            //saveAllRecommendtoCSV();
            saveAverageRate();
            //recommender.test();
            //recommender.getUserHistory(11);
        } catch (Exception ex) {
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //test only one movie
//        Movie movie = recommender.getMovie(63);
//        System.out.println("Test recommendbasedContent: "+movie.getName());
//        ArrayList<Movie> results = recommender.recommendbasedContent(movie, 5);
//        System.out.print("recommendbasedContent: ");
//        for (int i = 0; i < 5; i ++) {
//            System.out.print(results.get(i).getName()+"    ");
//        }
        System.out.println();
    }
    
}
