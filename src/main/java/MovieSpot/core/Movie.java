/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.core;

import java.io.Serializable;
import java.util.ArrayList;
/**
 *
 * @author Administrator
 */
public class Movie implements Serializable {
    private String name;
    private int index;
    private double rate;
    private Director director;
    private Writer writer;
    private Ratings rating;
    private ArrayList<Genre> genres;
    private ArrayList<Star> stars;
    private Poster poster;
    private String keywords;
    private int release_year;

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass().equals(Integer.class)) {
            return this.index == (int) obj;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        if (this.index != other.index) {
            return false;
        }
        return true;
    }
    
    public Movie(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public Movie(int _index, String _name, int director_id, int writer_id, int rating_id, double _rate, String _keywords, int _release_year) {
        index = _index;
        name = _name;
        director = new Director(director_id);
        writer = new Writer(writer_id);
        rating = new Ratings(rating_id);
        genres = new ArrayList<>();
        stars = new ArrayList<>();
        poster = new Poster(index);
        rate = _rate;
        keywords = _keywords;
        release_year = _release_year;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public double getRate() {
        return rate;
    }

    public Director getDirector() {
        return director;
    }
    
    public String getDirectorName() {
        return Director.getName(director.index);
    }

    public Writer getWriter() {
        return writer;
    }
    
    public String getWriterName() {
        return Writer.getName(writer.index);
    }

    public Ratings getRating() {
        return rating;
    }
    
    public String getRatingName() {
        return Ratings.getName(rating.index);
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }
    
    public String getGenreNames() {
        ArrayList<String> res = new ArrayList<>(genres.size());
        String names = "";
        for (int i = 0; i < genres.size(); i++) {
            res.add(Genre.getName(genres.get(i).index));
        }
        for (int i = 0; i < genres.size(); i++)
        {
            if (i != genres.size()-1)
                names = names + res.get(i) + " | ";
            else
                names = names + res.get(i);
        }
        return names;
    }

    public ArrayList<Star> getStars() {
        return stars;
    }
    
    public String getStarNames() {
        ArrayList<String> res = new ArrayList<>(stars.size());
        String names = "<html>";
        for (int i = 0; i < stars.size(); i++) {
            res.add(Star.getName(stars.get(i).index));
        }
        for (int i = 0; i < stars.size(); i++)
        {
            if (i != stars.size()-1)
                names = names + res.get(i) + "<br />";
            else
                names = names + res.get(i);
        }
        names += "</html>";
        return names;
    }

    public int getRelease_year() {
        return release_year;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public void setWriter(Writer writer) {
        this.writer = writer;
    }

    public void setRating(Ratings rating) {
        this.rating = rating;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }

    public void setStars(ArrayList<Star> stars) {
        this.stars = stars;
    }

    public void setRelease_year(int release_year) {
        this.release_year = release_year;
    }

    void addGenre(int genre_id) {
        genres.add(new Genre(genre_id));
    }

    void addStar(int star_id) {
        stars.add(new Star(star_id));
    }

    public Poster getPoster() {
        return poster;
    }
    
    
    
}
