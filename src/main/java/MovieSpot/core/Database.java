/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.core;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Administrator
 */
public class Database {
    private static final String QUERY_MOVIE = "SELECT movie.id, movie.name, director.director_id, "
            + "writer.writer_id, rating.rating_id, movie.rate, movie.keywords, movie.release_year FROM url_table AS movie, "
            + "movie_director AS director, movie_writer AS writer, movie_rating AS rating "
            + "WHERE movie.id = director.movie_id AND movie.id = writer.movie_id "
            + "AND movie.id = rating.movie_id";
    private static final String QUERY_GENRE = "SELECT genre.genre_id FROM url_table AS movie, movie_genre AS genre "
            + "WHERE movie.id = genre.movie_id and movie.id = ?";
    private static final String QUERY_STAR = "SELECT star.star_id FROM url_table AS movie, movie_star AS star "
            + "WHERE movie.id = star.movie_id and movie.id = ?";
    private static final String QUERY_KEYWORD = "";
    private String file_path;
    private SQLiteConnection db;
    private SQLiteStatement queryMovie;
    private SQLiteStatement queryStar;
    private SQLiteStatement queryGenre;
    private SQLiteStatement queryKeyword;
    boolean inited;
    
    public Database(String file) {
        file_path = file;
        inited = false;
    }
    
    public void init(String file) {
        try {
            file_path = file;
            db = new SQLiteConnection(new File(file));
            db.open(true);
            queryMovie = db.prepare(QUERY_MOVIE);
            queryStar = db.prepare(QUERY_STAR);
            queryGenre = db.prepare(QUERY_GENRE);
            //queryKeyword = db.prepare(QUERY_KEYWORD);
            
            SQLiteStatement st = db.prepare("SELECT id, name FROM director_table");
            while(st.step()) {
                Director.push(st.columnInt(0), st.columnString(1));
            }
            st.dispose();
            st = db.prepare("SELECT id, name FROM writer_table");
            while(st.step()) {
                Writer.push(st.columnInt(0), st.columnString(1));
            }
            st.dispose();
            st = db.prepare("SELECT id, rating FROM rating_table");
            while(st.step()) {
                Ratings.push(st.columnInt(0), st.columnString(1));
            }
            st.dispose();
            st = db.prepare("SELECT id, name FROM star_table");
            while(st.step()) {
                Star.push(st.columnInt(0), st.columnString(1));
            }
            st.dispose();
            st = db.prepare("SELECT id, genre FROM genre_table");
            while(st.step()) {
                Genre.push(st.columnInt(0), st.columnString(1));
            }
            st.dispose();
            
            inited = true;
        } catch(Exception ex) {
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void init()  {
        if (file_path != null)
            init(file_path);
    }  

    ArrayList<Movie> getMovies() throws SQLiteException {
        ArrayList<Movie> results = new ArrayList<>();
        while (queryMovie.step()) {
            Movie new_movie = new Movie(queryMovie.columnInt(0), queryMovie.columnString(1), 
                    queryMovie.columnInt(2), queryMovie.columnInt(3), queryMovie.columnInt(4),
                    queryMovie.columnDouble(5), queryMovie.columnString(6), queryMovie.columnInt(7));
            results.add(new_movie);
        }
        queryMovie.reset();
        
        //add stars
        for (Movie movie:results) {
            queryStar.bind(1, movie.getIndex());
            while(queryStar.step()) {
                movie.addStar(queryStar.columnInt(0));
            }
            queryStar.reset();
        }
        //add genres
        for (Movie movie:results) {
            queryGenre.bind(1, movie.getIndex());
            while(queryGenre.step()) {
                movie.addGenre(queryGenre.columnInt(0));
            }
            queryGenre.reset();
        }
        //add keywords
        
        return results;
    }
}
