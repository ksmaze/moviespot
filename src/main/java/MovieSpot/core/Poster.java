/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class Poster {
    private final static String POSTER_PATH = "./posters/";
    
    private static int FeatureNumber = 240;
    private ArrayList<Integer> feature;
    
    public static void setFeatureNumber(int FeatureNumber) {
        Poster.FeatureNumber = FeatureNumber;
    }

    public static int getFeatureNumber() {
        return FeatureNumber;
    }
    
    public Poster() {
        feature = new ArrayList<>(FeatureNumber);
    }
    
    public Poster(int idx) {
        feature = new ArrayList<>(FeatureNumber);
        load(POSTER_PATH+idx+".txt");
    }
    
    public void load(String filename) {
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.useDelimiter(",");
            while(scanner.hasNext()) {
                feature.add(scanner.nextInt());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Poster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Integer> getFeature() {
        return feature;
    }
    
    
    
}
