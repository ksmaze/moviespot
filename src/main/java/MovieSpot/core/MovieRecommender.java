/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.core;

import MovieSpot.util.LDDM;
import MovieSpot.util.CustomizeDistance;
import MovieSpot.util.WeightedDistance;
import MovieSpot.util.ensembleLDDM;
import com.almworks.sqlite4java.SQLiteException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.grouplens.lenskit.GlobalItemRecommender;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.RatingPredictor;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderEngine;
import org.grouplens.lenskit.baseline.BaselinePredictor;
import org.grouplens.lenskit.baseline.ItemUserMeanPredictor;
import org.grouplens.lenskit.collections.ScoredLongList;
import org.grouplens.lenskit.core.LenskitRecommender;
import org.grouplens.lenskit.core.LenskitRecommenderEngine;
import org.grouplens.lenskit.core.LenskitRecommenderEngineFactory;
import org.grouplens.lenskit.data.Event;
import org.grouplens.lenskit.data.UserHistory;
import org.grouplens.lenskit.data.dao.DataAccessObject;
import org.grouplens.lenskit.data.dao.SimpleFileRatingDAO;
import org.grouplens.lenskit.data.event.Rating;
import org.grouplens.lenskit.knn.item.ItemItemGlobalRecommender;
import org.grouplens.lenskit.knn.item.ItemItemRatingPredictor;
import org.grouplens.lenskit.knn.item.ItemItemRecommender;
import org.grouplens.lenskit.knn.item.ItemSimilarity;
import org.grouplens.lenskit.knn.params.NeighborhoodSize;
import org.grouplens.lenskit.knn.user.UserUserRecommender;
import org.grouplens.lenskit.params.Damping;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.MeanVarianceNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.VectorNormalizer;
import org.grouplens.lenskit.util.io.CompressionMode;
import sun.misc.REException;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instance;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.Bagging;
import weka.classifiers.meta.Vote;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.DistanceFunction;
import weka.core.EuclideanDistance;
import weka.core.Instances;
import weka.core.ManhattanDistance;
import weka.core.SelectedTag;
import weka.core.neighboursearch.LinearNNSearch;
import weka.filters.unsupervised.attribute.StringToWordVector;
/**
 *
 * @author Administrator
 */
public class MovieRecommender {
    private final static String DATABASE_PATH = "movie100k.db";
    private final static String USER_DATA_PATH = "./ml100k/u.data";
    //private final static String POSTER_PATH = "./posters/";
    private ArrayList<Movie> movies;
    private Instances instances;
    ArrayList<Attribute> attributes;
    Database database;
    //LDDM recommender;
    Vote recommender;
    LenskitRecommenderEngineFactory factory;
    LenskitRecommenderEngine engine;
    LenskitRecommender rec;
    LinearNNSearch search;
    ArrayList<Integer> neighbors;
    boolean updated;
    int default_k;
    private double DIRECTOR_W = 4;
    private double WRITER_W = 1;
    private double RATING_W = 100;
    private double GENRE_W = 10000;
    private double STAR_W = 1000;
    private double POSTER_W = 1;
    private boolean use_poster = true;
    
    private void init() {
        database = new Database(DATABASE_PATH);
        try {
            database.init();
            movies = database.getMovies();
            factory = new LenskitRecommenderEngineFactory();
            factory.setDAOFactory(new SimpleFileRatingDAO.Factory(new File(USER_DATA_PATH), "\t"));
            factory.set(NeighborhoodSize.class).to(30);
            factory.in(ItemSimilarity.class).set(Damping.class).to(100.0d);
            factory.bind(GlobalItemRecommender.class).to(ItemItemGlobalRecommender.class);
            factory.bind(ItemRecommender.class).to(UserUserRecommender.class);
            
            factory.bind(RatingPredictor.class).to(ItemItemRatingPredictor.class);
            factory.bind(VectorNormalizer.class).to(MeanVarianceNormalizer.class);
            factory.bind(BaselinePredictor.class).to(ItemUserMeanPredictor.class);
            factory.bind(UserVectorNormalizer.class).to(BaselineSubtractingUserVectorNormalizer.class);
            engine = factory.create();
            
            rec = engine.open();
            updated = false;
            default_k = 5;
            //recommender = new LDDM();
            //recommender = new ensembleLDDM();
            recommender = new Vote();
            search = new LinearNNSearch();
        } catch (Exception ex) {
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public MovieRecommender() {
        init();
    }
    
    public MovieRecommender(boolean _use_poster) {
        use_poster = _use_poster;
        init();
    }
    
    public void transInstance()  {
        attributes = new ArrayList<>();
        Attribute attribute;
        attribute = new Attribute("id");
        //attribute.setWeight(0);
        attributes.add(attribute);
        //attributes.add(new Attribute("name", (List<String>)null));     //nominal
        List<String> director_ids = new ArrayList<>();
        for (Integer director_id:Director.getIndex_table().keySet()) {
            director_ids.add(director_id.toString());
        }
        attribute = new Attribute("director", director_ids);
        //attribute.setWeight(DIRECTOR_W);
        attributes.add(attribute);   //nominal
        List<String> writer_ids = new ArrayList<>();
        for (Integer writer_id:Writer.getIndex_table().keySet()) {
            writer_ids.add(writer_id.toString());
        }
        attribute = new Attribute("writer", writer_ids);
        //attribute.setWeight(WRITER_W);
        attributes.add(attribute);   //nominal
        
        List<String> rating_ids = new ArrayList<>();
        for (Integer rating_id:Ratings.getIndex_table().keySet()) {
            rating_ids.add(rating_id.toString());
        }
        attribute = new Attribute("rating", rating_ids);
        //attribute.setWeight(RATING_W);
        attributes.add(attribute);   //nominal

        
        /*
         * maybe try another method: if one feature occurs multiple times, then set it as an individual feature
         * otherwise, collapse them as one feature as other_star
         */
        List<String> binary_value = new ArrayList<>(2);
        binary_value.add("f");
        binary_value.add("t");
        //add genre as binary
        for (Integer genre_id:Genre.getIndex_table().keySet()) {
            attribute = new Attribute("G"+genre_id.toString(), binary_value);
            //attribute.setWeight(GENRE_W/Genre.getIndex_table().size());
            attributes.add(attribute);   //binary
        }
        
        /*
         * list the arrangement for 3 stars, that will be only 9 attribute
         */
        //add star as nominal
        ArrayList<String> stars_id = new ArrayList<>(Star.getIndex_table().size());
        for (Integer star_id:Star.getIndex_table().keySet()) {
            stars_id.add(String.valueOf(star_id));
        }
        
        for (int i = 0; i < 18; i++) {
            attribute = new Attribute("S"+i, stars_id);
            attributes.add(attribute);
        }
        
        //attribute.setWeight(STAR_W/Genre.getIndex_table().size());
        //attributes.add(attribute);   //binary
        //add poster visual feature as num
        if (use_poster) {
            for (int i = 0; i < Poster.getFeatureNumber(); i++) {
                attribute = new Attribute("P"+i);
                attribute.setWeight(POSTER_W/Genre.getIndex_table().size());
                attributes.add(attribute);
            }
        }
        attributes.add(new Attribute("rate"));               //num
        instances = new Instances("KNN", attributes, movies.size());
        instances.setClassIndex(instances.numAttributes()-1);
        
        //trans
        for (Movie movie:movies) {
            try {
            Instance instance = new DenseInstance(attributes.size());
            instance.setDataset(instances);
            instance.setValue(0, movie.getIndex());
            instance.setValue(1, Integer.toString(movie.getDirector().index));
            instance.setValue(2, Integer.toString(movie.getWriter().index));
            instance.setValue(3, Integer.toString(movie.getRating().index));
            
            int start = 4;
            int i;
            for (i = 0; i < Genre.getIndex_table().size(); i++) {
                instance.setValue(start+i, "f");
            }
            for (i = 0; i < movie.getGenres().size(); i++) {
                instance.setValue(instances.attribute("G"+movie.getGenres().get(i).index), "t");
            }
            start += Genre.getIndex_table().size();
            //for (i = 0; i < Star.getIndex_table().size(); i++) {
            //    instance.setValue(start+i, "f");
            //}
            if (movie.getStars().size() >= 1) {
                instance.setValue(start, movie.getStars().get(0).index);
                instance.setValue(start+4, movie.getStars().get(0).index);
                instance.setValue(start+8, movie.getStars().get(0).index);
            }
            if (movie.getStars().size() >= 2) {
                instance.setValue(start+1, movie.getStars().get(1).index);
                instance.setValue(start+5, movie.getStars().get(1).index);
                instance.setValue(start+6, movie.getStars().get(1).index);
            }
            if (movie.getStars().size() >= 3) {
                instance.setValue(start+2, movie.getStars().get(2).index);
                instance.setValue(start+3, movie.getStars().get(2).index);
                instance.setValue(start+7, movie.getStars().get(2).index);
            }
            start += 9;
            //for (i = 0; i < movie.getStars().size(); i++) {
            //    instance.setValue(instances.attribute("S"+movie.getStars().get(i).index), "t");
            //}
            //start += Star.getIndex_table().size();
            if (use_poster) {
                for (i = 0; i < Poster.getFeatureNumber(); i++) {
                    instance.setValue(start+i, (double)movie.getPoster().getFeature().get(i));
                }
                start += Poster.getFeatureNumber();
            }
            instance.setValue(start, movie.getRate()*0.38);
            instance.setClassValue(movie.getRate()*0.38);
            instances.add(instance);
            } catch (Exception ex ){
                System.err.println("movie id: "+movie.getIndex()+" actors: "+movie.getStars().size());
            }
        }
    }
    
    public void buildNeighbors() {
        if (neighbors == null) {
          neighbors = new ArrayList<>();  
          
        } 
    }
    
    public void buildClassifier() {
        try {
            //recommender.setDistanceWeighting (new SelectedTag (IBk.WEIGHT_INVERSE,
            //        IBk.TAGS_WEIGHTING));
            DistanceFunction df = new CustomizeDistance(instances);
            
            search.setDistanceFunction(df);
            search.setInstances(instances);
            
            //recommender.setNearestNeighbourSearchAlgorithm(search);
            //recommender.buildClassifier(instances);
            //recommender.buildClassifier(instances,5);
//            LDDM[] classifiers = new LDDM[instances.size()];
//            for (int i = 0; i < instances.size(); i++) {
//                classifiers[i] = new LDDM();
//                classifiers[i].buildClassifier(instances, i);
//            }
            
            LDDM[] classifiers = new LDDM[5];
            Random seed = new Random();
            for (int i = 0; i < 5; i++) {
                classifiers[i] = new LDDM();
                classifiers[i].buildClassifier(instances, seed.nextInt(instances.size()));
            }
            
            //recommender.setClassifier(new LDDM());
            recommender.setClassifiers(classifiers);
            //recommender.buildClassifier(instances);
            
            
            updated = true;
            
        } catch (Exception ex) {
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     *
     */
    public void build() {
        if (!updated) {
            transInstance();
            buildClassifier();
        }
    }
    
    public void test() {
        if (!updated) {
            build();
        }
        try {
                 Evaluation eva = new Evaluation(instances);
                 
                 eva.crossValidateModel(recommender, instances, 10, new Random(1));
                 System.out.println(eva.toSummaryString());
            } catch (Exception ex) {
                 Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public ArrayList<Movie> recommendbasedContent(Movie test, int k) {
        ArrayList<Movie> ans = new ArrayList<>(k);
        try {
            Instance test_instance = search.getInstances().instance(movies.indexOf(test));
            Instances results = search.kNearestNeighbours(test_instance, k);
            for (int i = 0; i < results.size(); i++) {
                ans.add(findMovie((int)results.get(i).value(0)));
            }
        } catch (Exception ex) {
            ans = null;
            //Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ans;
    }
    
    public ArrayList<Movie> recommendbasedContent(int movie_id, int k) {
        ArrayList<Movie> ans = new ArrayList<>(k);
        try {
            Instance test_instance = search.getInstances().instance(findMovieIndex(movie_id));
            Instances results = search.kNearestNeighbours(test_instance, k);
            for (int i = 0; i < results.size(); i++) {
                ans.add(findMovie((int)results.get(i).value(0)));
            }
        } catch (Exception ex) {
            ans = null;
            //Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ans;
    }
    
    
    public ArrayList<Movie> recommendbasedSocial(Movie test, int k) {
        ArrayList<Movie> ans = new ArrayList<>(k);
        try {
            Set<Long> testset = new HashSet<>();
            testset.add((long)test.getIndex());
            GlobalItemRecommender girecommender = rec.getGlobalItemRecommender();
            ScoredLongList reclist = girecommender.globalRecommend(testset, k);
            for (int i = 0; i < reclist.size(); i++) {
                Movie recMovie = findMovie(reclist.get(i).intValue());
                if (recMovie != null) {
                    ans.add(recMovie);
                }
            }
            
        } catch (Exception ex) {
            ans = null;
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ans;
    }
    
    public ArrayList<Movie> recommendbasedSocial(int movie_id, int k) {
        ArrayList<Movie> ans = new ArrayList<>(k);
        try {
            Set<Long> testset = new HashSet<>();
            testset.add((long)movie_id);
            GlobalItemRecommender girecommender = rec.getGlobalItemRecommender();
            ScoredLongList reclist = girecommender.globalRecommend(testset, k);
            for (int i = 0; i < reclist.size(); i++) {
                Movie recMovie = findMovie(reclist.get(i).intValue());
                if (recMovie != null) {
                    ans.add(recMovie);
                }
            }
            
        } catch (Exception ex) {
            ans = null;
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ans;
    }
    
    public ArrayList<Movie> recommendbasedSocialforUser(int userid, int k) {
        ArrayList<Movie> ans = new ArrayList<>(k);
        try {
            ItemRecommender irecommender = rec.getItemRecommender();
        
            ScoredLongList reclist = irecommender.recommend(userid, k);
            for (int i = 0; i < reclist.size(); i++) {
                Movie recMovie = findMovie(reclist.get(i).intValue());
                if (recMovie != null) {
                    ans.add(recMovie);
                }
            }
            
        } catch (Exception ex) {
            ans = null;
            Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ans;
    }
    
    public Movie getMovie(int idx) {
        return movies.get(idx);
    }
    
    public Movie findMovie(int idx) {
        for (Movie movie:movies) {
            if (movie.getIndex() == idx) {
                return movie;
            }
        }
        return null;
    }
    
    public int findMovieIndex(int idx) {
        for (int i = 0; i < movies.size(); i++) {
            if (movies.get(i).getIndex() == idx) {
                return i;
            }
        }
        return -1;
    }

    public ArrayList<Movie> getMovies() {
        return movies;
    }
    
    public ArrayList<Movie> getUserHistory(long userid) {
        DataAccessObject dao = rec.getDataAccessObject();
        UserHistory<Rating> history = dao.getUserHistory(userid, Rating.class);
        ArrayList<Movie> results = new ArrayList<>(history.size());
        for (int i = 0; i < history.size(); i++) {
            Movie movie = findMovie((int)history.get(i).getItemId());
            if (movie != null)
                results.add(movie);
        }
        return results;
    }
    
    public ArrayList<Movie> recommendbasedContentforUser(int userid, int k) throws Exception {
        ArrayList<Movie> history = getUserHistory(userid);
        ArrayList<Movie> rec = new ArrayList<>();
        LDDM recc = new LDDM();
        recc.buildClassifier(instances, findMovieIndex(history.get(0).getIndex()));
        ArrayList<Movie> ans = new ArrayList<>(k);
        LinearNNSearch search0 = new LinearNNSearch();
        WeightedDistance wd = new WeightedDistance();
        wd.setM_W(recc.getM_W());
        search0.setDistanceFunction(wd);
        search0.setInstances(instances);
        
        try {
            Instance test_instance = search0.getInstances().instance(findMovieIndex(history.get(0).getIndex()));
            Instances results = search0.kNearestNeighbours(test_instance, k);
            for (int i = 0; i < results.size(); i++) {
                ans.add(findMovie((int)results.get(i).value(0)));
            }
        } catch (Exception ex) {
            ans = null;
            //Logger.getLogger(MovieRecommender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ans;
    }
}
