/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieSpot.core;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class Keyword {
    private static HashMap<String, Integer> name_table = new HashMap<>();
    private static HashMap<Integer, String> index_table = new HashMap<>();
    String name;
    int index;
    
    public static void push(int _index, String _name) {
        name_table.put(_name, _index);
        index_table.put(_index, _name);
    }
    
    public static HashMap<String, Integer> getName_table() {
        return name_table;
    }

    public static HashMap<Integer, String> getIndex_table() {
        return index_table;
    }
    
    public static String getName(int idx) {
        return index_table.get(idx);
    }

    public Keyword(int _index) {
        index = _index;
    }
}
